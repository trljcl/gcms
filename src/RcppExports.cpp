// This file was generated by Rcpp::compileAttributes
// Generator token: 10BE3573-1514-4C36-9D1C-5A225CD40393

#include <Rcpp.h>

using namespace Rcpp;

// agilentImportMSFile
List agilentImportMSFile(std::string file);
RcppExport SEXP GCMS_agilentImportMSFile(SEXP fileSEXP) {
BEGIN_RCPP
    Rcpp::RObject __result;
    Rcpp::RNGScope __rngScope;
    Rcpp::traits::input_parameter< std::string >::type file(fileSEXP);
    __result = Rcpp::wrap(agilentImportMSFile(file));
    return __result;
END_RCPP
}
// agilentImportFromDir
List agilentImportFromDir(std::string directory);
RcppExport SEXP GCMS_agilentImportFromDir(SEXP directorySEXP) {
BEGIN_RCPP
    Rcpp::RObject __result;
    Rcpp::RNGScope __rngScope;
    Rcpp::traits::input_parameter< std::string >::type directory(directorySEXP);
    __result = Rcpp::wrap(agilentImportFromDir(directory));
    return __result;
END_RCPP
}
